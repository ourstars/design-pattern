package shoo.design.builder;

/**
 * @author shoo on 2017/7/16 19:06.
 */
public class BuilderTest {
    public static void main(String[] args) {
        NutritionFacts nutri = new NutritionFacts.Builder(200,2).fat(100).calories(300).sodium(400).build();
        System.out.println(nutri);

        NutritionFactsTwo two = new NutritionFactsTwo
                .Creator(200, 20)
                .calories(2)
                .fat(3)
                .carbohydrate(4)
                .sodium(5)
                .create();
        System.out.println(two);
    }


}
