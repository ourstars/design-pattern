package shoo.design.builder;

/**
 * @author shoo on 2017/7/16 18:42.
 * builder
 */
public class NutritionFactsTwo {
    private int servingSize;
    private int servings;
    private int calories;
    private int fat;
    private int sodium;
    private int carbohydrate;


    private NutritionFactsTwo(int servingSize, int servings) {
        this.servingSize = servingSize;
        this.servings = servings;
    }

    public static class Creator{

        private NutritionFactsTwo client;

        public  Creator(int servingSize, int servings){
            this.client = new NutritionFactsTwo(servingSize, servings);
        }


        public Creator calories(int val){
            this.client.calories = val;
            return this;
        }

        public Creator fat(int val){
            this.client.fat = val;
            return this;
        }
        public Creator carbohydrate(int val){
            this.client.carbohydrate = val;
            return this;
        }
        public Creator sodium(int val){
            this.client.sodium = val;
            return this;
        }
        public NutritionFactsTwo create(){
            return this.client;
        }

    }


    @Override
    public String toString() {
        return "NutritionFactsTwo{" +
                "servingSize=" + servingSize +
                ", servings=" + servings +
                ", calories=" + calories +
                ", fat=" + fat +
                ", sodium=" + sodium +
                ", carbohydrate=" + carbohydrate +
                '}';
    }
}
