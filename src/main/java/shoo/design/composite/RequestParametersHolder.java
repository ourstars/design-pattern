package shoo.design.composite;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 2023/3/12 11:12 AM
 *
 * @author shoo
 * @describe
 */
public class RequestParametersHolder {
    private ConcurrentHashMap protocalMustParams;
    private ConcurrentHashMap protocalOptParams;
    private ConcurrentHashMap applicationParams;

    public RequestParametersHolder() {
    }

    public ConcurrentHashMap getProtocalMustParams() {
        return this.protocalMustParams;
    }

    public void setProtocalMustParams(ConcurrentHashMap protocalMustParams) {
        this.protocalMustParams = protocalMustParams;
    }

    public ConcurrentHashMap getProtocalOptParams() {
        return this.protocalOptParams;
    }

    public void setProtocalOptParams(ConcurrentHashMap protocalOptParams) {
        this.protocalOptParams = protocalOptParams;
    }

    public ConcurrentHashMap getApplicationParams() {
        return this.applicationParams;
    }

    public void setApplicationParams(ConcurrentHashMap applicationParams) {
        this.applicationParams = applicationParams;
    }
}
