package shoo.design.singleTon;

/**
 * @author shoo on 2017/7/15 1:07.
 * @describe 单例模式，饿汉式
 */
public class SingleTon {
    private static final SingleTon singleTon = new SingleTon();
    //通过构造函数私有化限制通过new获得类的实例
    private SingleTon(){

    };
    //通过该静态方法向外界提供实例
    public static SingleTon getSingleTon(){
        return singleTon;
    }
    //类中的其它方法
    public static void doSomeThing(){

    }
}
