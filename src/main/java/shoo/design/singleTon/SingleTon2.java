package shoo.design.singleTon;

/**
 * 2023/3/11 5:21 PM
 *
 * @author shoo
 * @describe 单例模式 懒汉式
 */
public class SingleTon2 {

    private static  SingleTon2 singleTon2;
    //通过构造函数私有化限制通过new获得类的实例
    private SingleTon2(){

    };
    //通过该静态方法向外界提供实例
    public static SingleTon2 getSingleTon2(){

        if(singleTon2 == null){
            synchronized (SingleTon2.class){
                if(singleTon2 == null){
                    singleTon2 = new SingleTon2();
                }
            }
        }

        return singleTon2;
    }
    //类中的其它方法

}
