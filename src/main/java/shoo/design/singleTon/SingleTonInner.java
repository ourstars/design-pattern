package shoo.design.singleTon;

/**
 * 2023/3/11 5:32 PM
 *
 * @author shoo
 * @describe 单例模式：静态内部类实现
 */
public class SingleTonInner {

    // 私有化构造函数
    private SingleTonInner(){

    }

    // 内部类，第一次调用时才初始化
    private static class SingleHolder{
        private static final SingleTonInner instance = new SingleTonInner();
    }

    // 通过内部类获得实例
    public SingleTonInner getInstance(){
        return SingleHolder.instance;
    }

}
