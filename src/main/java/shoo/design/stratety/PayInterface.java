package shoo.design.stratety;

/**
 * 2023/3/11 3:49 PM
 *
 * @author shoo
 * @describe 抽象接口，定义付款方法
 */
interface  PayInterface {

    String pay();

}
