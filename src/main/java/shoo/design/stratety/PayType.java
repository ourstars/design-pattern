package shoo.design.stratety;

/**
 * 2023/3/11 3:41 PM
 *
 * @author shoo
 * @describe
 */
public enum PayType {

    TLPay(1, "app"),
    WECHAT(2,"wechat");

    private final Integer type;
    private final String info;

    PayType(Integer type, String info)
    {
        this.type = type;
        this.info = info;
    }

    public Integer getType()
    {
        return type;
    }

    public String getInfo()
    {
        return info;
    }
}
