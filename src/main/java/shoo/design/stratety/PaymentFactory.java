package shoo.design.stratety;

import java.util.HashMap;
import java.util.Map;

/**
 * 2023/3/11 3:48 PM
 *
 * @author shoo
 * @describe 简单工厂：获得付款客户端
 */
public class PaymentFactory {

    private static final Map<Integer, PayInterface> payStrategies = new HashMap<>();

    static {
        payStrategies.put(PayType.TLPay.getType(), new WxPayment());
        payStrategies.put(PayType.WECHAT.getType(), new AliPayment());

    }

    public static PayInterface getPayment(Integer type) {
        if (type == null) {
            throw new IllegalArgumentException("pay type is empty.");
        }
        if (!payStrategies.containsKey(type)) {
            throw new IllegalArgumentException("pay type not supported.");
        }
        return payStrategies.get(type);
    }
}
