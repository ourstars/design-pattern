package shoo.design.stratety;

/**
 * 2023/3/11 4:32 PM
 *
 * 策略模式 + 简单工厂 重构付款代码
 *
 * @author shoo
 * @describe 业务逻辑代码
 */
public class StrategyTest {

    public static void main(String[] args) {
        // 正式环境，type由参数传入
        Integer type = PayType.WECHAT.getType();

        PayInterface pay = PaymentFactory.getPayment(type);
        pay.pay();

    }
}
