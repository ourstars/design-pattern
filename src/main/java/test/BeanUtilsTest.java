package test;


import net.sf.cglib.beans.BeanCopier;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.BeanUtils;

/**
 * 2022/2/25 3:10 PM
 *
 * @author shoo
 * @describe 各个BeanUtils性能比较
 *           1 spring
 *           2 Apache
 *           3 cglib
 */
public class BeanUtilsTest {

    public static void main(String[] args) throws Exception{
        BeanUtilsTest test = new BeanUtilsTest();
        test.test(100000,1);
        test.test(100000,2);
        test.test(100000,3);
    }


    public void test(int times, int method )throws Exception{
        Person person1 = new Person();
        person1.setName("xx");
        person1.setAge(18);
        person1.setSex(0);
        person1.setHigh(1.68F);
        Person person2 = new Person();
        //计时
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();
        for(int i = 0;i<=times;i++ ){
            MyBeanUtils.copyProprities(person1,person2,method);
        }
        long time = stopwatch.getTime();
        System.out.println("方法：" + method + "，" + times + "次，" + "耗时" + time);
    }



    public static class MyBeanUtils{
        private static void copyProprities(Object source, Object target, int method) throws Exception{
            if(method==1){
                BeanUtils.copyProperties(source, target);
            }
            if(method==2){
                org.apache.commons.beanutils.BeanUtils.copyProperties(target, source);
            }
            if(method==3){
                BeanCopier copier = BeanCopier.create(Person.class, Person.class, false);
                copier.copy(source, target, null);
            }
        }
    }

    public class Person{
        private String name;
        private int age;
        private int sex;
        private float high;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public float getHigh() {
            return high;
        }

        public void setHigh(float high) {
            this.high = high;
        }
    }

}
